###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                 PID - PID Control                                        ##  
##                                                                                          ##
##                                                                                          ##
############################         Version: 1.5             ################################
# Author:  Attila Horvath
# Date:    10-03-2023
# Version: v1.5
# Changes: Setpoint ramping functionality added 
############################         Version: 1.4             ################################
# Author:  Attila Horvath
# Date:    08-03-2023
# Version: v1.4
# Changes: Automatic Profile Selector functionality added from the OPI  
############################         Version: 1.3             ################################
# Author:  Attila Horvath
# Date:    08-08-2022
# Version: v1.3
# Changes: Archived PVs updated   
############################           Version: 1.2           ################################
# Author:	Miklos Boros 
# Date:		08-04-2010
# Version:  v1.2
# Changes:  Removed Forced mode, added Regulation ON/OFF
############################           Version: 1.0           ################################ 
# Author:	Miklos Boros
# Date:		01-09-2019
# Version:  v1.0
# Changes:  First release in TestStand2
##############################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()


#Operation modes
add_digital("OpMode_Auto",   					ARCHIVE=True,             				   PV_DESC="Operation Mode Auto",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Manual",   					ARCHIVE=True,           				   PV_DESC="Operation Mode Manual",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("Regulation",   					ARCHIVE=True,              				   PV_DESC="PID Function State",        PV_ONAM="ON",                            PV_ZNAM="OFF")
add_analog("PID_Color",                "INT",                               			   PV_DESC="BlockIcon color")
add_digital("AutoProfileSel",   				ARCHIVE=True,              				   PV_DESC="Automatic Profile State",	PV_ONAM="ON",                            PV_ZNAM="OFF")
add_analog("AutoProfileColor",         "INT",                               			   PV_DESC="OPI color update")

#PID states
add_analog("SelectedPV",               "INT",   ARCHIVE=True,                              PV_DESC="Selected Measurement")
add_analog("LMN",                      "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value",             PV_EGU="%")
add_analog("LMN_P",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value P-Action")
add_analog("LMN_I",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value I-Action")
add_analog("LMN_D",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value D-Action")
add_analog("PID_DIF",                  "REAL",  ARCHIVE=True,                              PV_DESC="Actual PID Error/Difference")
add_analog("PV",                       "REAL",  ARCHIVE=True,                              PV_DESC="PID Process Value")
add_analog("MAN_SP",                   "REAL",  ARCHIVE=True,                              PV_DESC="PID Setpoint Value")
add_analog("PID_Cycle",                "INT",                               PV_DESC="PID cycle time",                PV_EGU="ms")
add_string("ProcValueName", 39,         PV_NAME="ProcValueName",  ARCHIVE=True,            PV_DESC="PV Name of the Process Value")
add_string("ProcValueEGU", 6,           PV_NAME="ProcValueEGU",  ARCHIVE=True,             PV_DESC="EGU of the Process Value")
add_string("FeedbackName1", 39,         PV_NAME="FeedbackName1",  ARCHIVE=True,     	   PV_DESC="Feedback Device 1 Name")
add_string("FeedbackName2", 39,         PV_NAME="FeedbackName2",  ARCHIVE=True,     	   PV_DESC="Feedback Device 2 Name")
add_string("FeedbackName3", 39,         PV_NAME="FeedbackName3",  ARCHIVE=True,     	   PV_DESC="Feedback Device 3 Name")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",                 PV_ZNAM="AllowManual")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking",                PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("MoveInterlock",  ARCHIVE=True,           									   PV_DESC="Move Interlock",            PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupInterlock",          													   PV_DESC="Group Interlock",           PV_ONAM="True",                          PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",        PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnablePIDConf",           PV_DESC="Enable PID Configuration",  PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnaFeedbackConf",         PV_DESC="Enable PID Configuration",  PV_ONAM="True",                          PV_ZNAM="False")
add_digital("LatchAlarm",              PV_DESC="Enable Alarm Latching",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")

#Block Icon controls
add_digital("EnableBlkCtrl",           PV_DESC="Enable Block SP Button",     PV_ONAM="True",             PV_ZNAM="False")

#Alarm signals
add_digital("LMN_HLIM",                                                     PV_ZNAM="True")
add_digital("LMN_LLIM",                                                     PV_ZNAM="True")
add_digital("Discrepancy_Error", 											PV_ZNAM="True")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")

#Warning signals
add_minor_alarm("ContDeviceInMan",     "ContDeviceInMan",                   PV_ZNAM="True")

#Feedbacks
add_analog("FB_Setpoint",              "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Setpoint from HMI (SP)")
add_analog("FB_Step",                  "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Step value for Open/Close",  PV_EGU="%")
add_analog("FB_Manipulated",           "REAL" ,  ARCHIVE=True,                             PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")

#Actual PID Profile
add_digital("Profile1",  ARCHIVE=True,                                                     PV_ZNAM="True")
add_digital("Profile2",  ARCHIVE=True,                                                     PV_ZNAM="True")
add_digital("Profile3",  ARCHIVE=True,                                                     PV_ZNAM="True")

#Selected PID Profile
add_analog("FB_Gain",                  "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI",                    "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD",                    "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Derivative action time",     PV_EGU="msec")
 
add_analog("FB_DEADB",                 "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM",              "REAL" ,  ARCHIVE=True,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM",              "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Low limit for MV")

#PID Profile 1
add_analog("FB_Gain_1",                "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_1",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_1",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_1",               "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_1",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_1",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Low limit for MV")


#PID Profile 2
add_analog("FB_Gain_2",                "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_2",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_2",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_2",               "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_2",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_2",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Low limit for MV")

#PID Profile 3
add_analog("FB_Gain_3",                "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_3",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_3",                  "DINT" ,  ARCHIVE=True,                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_3",               "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_3",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_3",            "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Low limit for MV")


add_digital("FB_P_1_ActionState",       PV_DESC="Proportional Action State")
add_digital("FB_P_2_ActionState",       PV_DESC="Proportional Action State")
add_digital("FB_P_3_ActionState",       PV_DESC="Proportional Action State")
add_digital("FB_I_1_ActionState",      	PV_DESC="Integral Action State")
add_digital("FB_I_2_ActionState",      	PV_DESC="Integral Action State")
add_digital("FB_I_3_ActionState",      	PV_DESC="Integral Action State")
add_digital("FB_D_1_ActionState",       PV_DESC="Differential Action State")
add_digital("FB_D_2_ActionState",       PV_DESC="Differential Action State")
add_digital("FB_D_3_ActionState",       PV_DESC="Differential Action State")

#Standby Synchroniztaion
add_digital("FB_SS_State",             PV_DESC="Standby Synchroniztaion State")
add_analog("FB_SS_Value",              "REAL" ,                             PV_DESC="Standby Synchroniztaion Value", PV_EGU="%")


#Setpoint Ramping
add_analog("MaxRampUPSpd","REAL",                    PV_DESC="Maximum Ramping UP Speed",                 PV_EGU="[PLCF#EGU]/min")
add_analog("MaxRampDNSpd","REAL",                    PV_DESC="Maximum Ramping DOWN Speed",               PV_EGU="[PLCF#EGU]/min")
add_analog("ActRampSpeed","REAL",                    PV_DESC="Actual Ramping Speed",                     PV_EGU="[PLCF#EGU]/min")
add_digital("Ramping",                               PV_DESC="Ramping Indicator",                        PV_ONAM="True",                                     PV_ZNAM="False")
add_digital("RampSettingOK",                         PV_DESC="Ramping can be enabled",                   PV_ONAM="True",                                     PV_ZNAM="False")
add_digital("RampSPChangeWarn",                      PV_DESC="Ramping Setpoint change warning",          PV_ONAM="True",                                     PV_ZNAM="False")

add_analog("FB_RampUPTIME","INT",                    PV_DESC="Ramping UP time",                          PV_EGU="min")
add_analog("FB_RampUPRANGE","REAL",                  PV_DESC="Ramping UP range",                         PV_EGU="[PLCF#EGU]")
add_analog("FB_RampUPSPEED","REAL",                  PV_DESC="Ramp UP Speed",                         	 PV_EGU="[PLCF#EGU]/min")
add_analog("FB_RampUPSPEED_C","REAL",                PV_DESC="Current Ramp UP Speed",                    PV_EGU="[PLCF#EGU]/min")
add_analog("FB_RampDNTIME","INT",                    PV_DESC="Ramping DOWN time",                        PV_EGU="min")
add_analog("FB_RampDNRANGE","REAL",                  PV_DESC="Ramping DOWN range",                       PV_EGU="[PLCF#EGU]")
add_analog("FB_RampDNSPEED","REAL",                  PV_DESC="Ramp UP Speed",                            PV_EGU="[PLCF#EGU]/min")
add_analog("FB_RampDNSPEED_C","REAL",                PV_DESC="Current Ramp Down Speed",                  PV_EGU="[PLCF#EGU]/min")
add_analog("FB_RampFinalSP","REAL",                  PV_DESC="Maximum Ramping UP Speed",                 PV_EGU="[PLCF#EGU]")
add_analog("FB_RampHoldDiff","REAL",  				 PV_DESC="Ramping Difference Hold",                  PV_EGU="[PLCF#EGU]")
add_analog("FB_RampHoldTime","REAL",  				 PV_DESC="Ramping Difference Time",                  PV_EGU="min")
add_digital("FB_HoldingON",   		    			 PV_DESC="Holding ON",								 				PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("FB_HoldingSPON",   		    		 PV_DESC="SP Holding ON",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_string("FB_HoldRemain", 39,                      PV_NAME="HoldRemain",                 PV_DESC="Holding Time Indication")

#Ramping Feedback
add_analog("FB_RampTIME","INT" ,                     PV_DESC="Ramping Time",                             PV_EGU="min")
add_analog("FB_RampRANGE","REAL" ,                   PV_DESC="Ramping Range",                            PV_EGU="[PLCF#EGU]")
add_digital("RampUP",   			ARCHIVE=True,    PV_DESC="Ramping UP Active",										PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("RampDOWN",   			ARCHIVE=True,    PV_DESC="Ramping DOWN Active",										PV_ONAM="ON",                    PV_ZNAM="OFF")

#OPI Visuals
add_digital("OPI_OnlyValve",   		    			 PV_DESC="Simplified OPI",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")

#Spare Signals
add_digital("Spare_Bool_1",   		    			 	PV_DESC="Spare_Bool_1",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("Spare_Bool_2",   		    			 	PV_DESC="Spare_Bool_2",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("Spare_Bool_3",   		    			 	PV_DESC="Spare_Bool_3",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("Spare_Bool_4",   		    			 	PV_DESC="Spare_Bool_4",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_digital("Spare_Bool_5",   		    			 	PV_DESC="Spare_Bool_5",								 			PV_ONAM="ON",                    PV_ZNAM="OFF")
add_analog("Spare_Real_1","REAL" ,						PV_DESC="Spare_Real_1")
add_analog("Spare_Real_2","REAL" ,						PV_DESC="Spare_Real_2")
add_analog("Spare_Real_3","REAL" ,						PV_DESC="Spare_Real_3")
add_analog("Spare_Real_4","REAL" ,						PV_DESC="Spare_Real_4")
add_analog("Spare_Real_5","REAL" ,						PV_DESC="Spare_Real_5")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                						PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              						PV_DESC="CMD: Manual Mode")

add_digital("Cmd_EnablePID_Conf",      						PV_DESC="CMD: Enable PID configuration from OPI")
add_digital("Cmd_DisablePID_Conf",     						PV_DESC="CMD: Disable PID configuration from OPI")

add_digital("Cmd_AckAlarm",            						PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_RampON",                            		PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",                           		PV_DESC="Turn Ramping OFF")
add_digital("Cmd_RampToFinal",                           	PV_DESC="Resume Ramping to final SP")
add_digital("Cmd_RampToCurrent",                           	PV_DESC="Resume Ramping to PV")

add_digital("Cmd_Hold_ON",                					PV_DESC="CMD: Hold Ramping ON")
add_digital("Cmd_Hold_OFF",               					PV_DESC="CMD: Hold Ramping OFF")

add_digital("Cmd_Regu_ON",             						PV_DESC="CMD: PID Function ON")
add_digital("Cmd_Regu_OFF",            						PV_DESC="CMD: PID Function OFF")

add_digital("Cmd_Profile1",            				PV_DESC="CMD: Select PID Profile 1")
add_digital("Cmd_Profile2",            				PV_DESC="CMD: Select PID Profile 2")
add_digital("Cmd_Profile3",            				PV_DESC="CMD: Select PID Profile 3")

#Spare Commands
add_digital("Cmd_Spare_1",             						PV_DESC="CMD: Spare_1")
add_digital("Cmd_Spare_2",             						PV_DESC="CMD: Spare_1")
add_digital("Cmd_Spare_3",             						PV_DESC="CMD: Spare_1")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
#PID Profile 1
add_analog("P_Gain_1",                 "REAL" ,  	ARCHIVE=True,							PV_DESC="PID Proportional gain")
add_analog("P_TI_1",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_1",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_1",                "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_1",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_1",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Low limit for MV")

add_digital("P_Prof1_P_ON",							ARCHIVE=True,                			PV_DESC="CMD: Enable P-Action")
add_digital("P_Prof1_I_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable I-Action")
add_digital("P_Prof1_D_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable D-Action")

#PID Profile 2
add_analog("P_Gain_2",                 "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Proportional gain")
add_analog("P_TI_2",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_2",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_2",                "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_2",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_2",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Low limit for MV")

add_digital("P_Prof2_P_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable P-Action")
add_digital("P_Prof2_I_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable I-Action")
add_digital("P_Prof2_D_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable D-Action")

#PID Profile 3
add_analog("P_Gain_3",                 "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Proportional gain")
add_analog("P_TI_3",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_3",                   "DINT" ,  	ARCHIVE=True,                           PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_3",                "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_3",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_3",             "REAL" ,  	ARCHIVE=True,                           PV_DESC="PID Low limit for MV")

add_digital("P_Prof3_P_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable P-Action")
add_digital("P_Prof3_I_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable I-Action")
add_digital("P_Prof3_D_ON",                			ARCHIVE=True,							PV_DESC="CMD: Enable D-Action")

#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",               "REAL" ,  	ARCHIVE=True,                           PV_DESC="Setpoint from HMI (SP)",        PV_EGU="%")
add_analog("P_Manipulated",            "REAL" ,  	ARCHIVE=True,                           PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step",                   "REAL" ,     ARCHIVE=True,                        	PV_DESC="Step value for open close",     PV_EGU="%")

#Automatic Profile Selection
add_analog("P_HystAboveSP",				"REAL" ,  	ARCHIVE=True,                           PV_DESC="Hysteresis Above SP")
add_analog("P_HystUnderSP",				"REAL" ,  	ARCHIVE=True,                           PV_DESC="Hysteresis Under SP")
add_analog("P_ProfAboveHyst",           "INT",    	ARCHIVE=True,                           PV_DESC="PID Profile Above Hyst")
add_analog("P_ProfBetweenHyst",         "INT",    	ARCHIVE=True,                           PV_DESC="PID Profile Between Hyst")
add_analog("P_ProfUnderHyst",           "INT",    	ARCHIVE=True,                           PV_DESC="PID Profile Under Hyst")

#Ramping speed from the HMI
add_analog("P_RampUPTIME","INT",  					ARCHIVE=True,							PV_DESC="Ramping UP Time",                          PV_EGU="min")
add_analog("P_RampUPRANGE","REAL",  				ARCHIVE=True,							PV_DESC="Ramping UP Range",                         PV_EGU="[PLCF#EGU]")
add_analog("P_RampDNTIME","INT",  					ARCHIVE=True,							PV_DESC="Ramping DOWN Time",                        PV_EGU="min")
add_analog("P_RampDNRANGE","REAL",  				ARCHIVE=True,							PV_DESC="Ramping DOWN Range",                       PV_EGU="[PLCF#EGU]")
add_analog("P_RampHoldDiff","REAL",  				ARCHIVE=True,							PV_DESC="Ramping Difference Hold",                  PV_EGU="[PLCF#EGU]")
add_analog("P_RampHoldTime","REAL",  				ARCHIVE=True,							PV_DESC="Ramping Difference Time",                  PV_EGU="sec")

add_digital("P_SelMeasDev1",						ARCHIVE=True,         					PV_DESC="CMD: Select Measured Device 1")
add_digital("P_SelMeasDev2",         				ARCHIVE=True,							PV_DESC="CMD: Select Measured Device 2")
add_digital("P_SelMeasDev3",         				ARCHIVE=True,							PV_DESC="CMD: Select Measured Device 3")

add_digital("P_AutoProf_ON",                		ARCHIVE=True,							PV_DESC="CMD: Enable Auto Profile")